import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Home from "./pages/Home"
import Billbox from "./pages/Billbox"
import Passbook from "./pages/Passbook"
import Profile from "./pages/Profile"
import Nav from "./Navigation/Nav"
import styled from "styled-components"
import More from "./pages/More"

export const Container = styled.div`
  width: 360px;
  margin: auto;
	background-color: #F6F6F6;
	margin-bottom: 56px;
`;

function App() {
	return (
		<Container>
			<Router>
				<Switch>
					<Route exact path="/passbook">
						<Passbook />
					</Route>
					<Route exact path="/billbox">
						<Billbox />
					</Route>
					<Route exact path="/profile">
						<Profile />
					</Route>
					<Route exact path="/home">
						<Home />
					</Route>
          <Route exact path="/more">
						<More />
					</Route>
					<Route exact path="/">
						<Home />
					</Route>
				</Switch>
        <Nav />
			</Router>
		</Container>
	)
}

export default App
