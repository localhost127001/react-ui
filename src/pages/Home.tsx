import React from "react"
import styled from "styled-components"
import Accounts from "../components/Home/Accounts"
import Services from "../components/Home/Services"
import Brands from "../components/Home/Brands"
import Transactions from "../components/Home/Transactions"

const HomeContainer = styled.div`
	overflow-y: auto;
	&::-webkit-scrollbar {
		width: 0px;
		background: transparent;
	}
`

const Home = () => {
	return (
		<HomeContainer>
			<Accounts />
			<Services />
			<Brands />
			<Transactions />
		</HomeContainer>
	)
}

export default Home
