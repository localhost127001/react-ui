import React, { Fragment, ReactNode } from "react"
import styled from "styled-components"

type TextProps = {
	size: "xxs" | "xs" | "sm" | "md" | "lg" | "xl" | "xxl"
	color?: string
	children: ReactNode
	opacity?: number
}

const variant = (size: string) => {
	let fontSize = 0, lineHeight = 0, fontWeight = 0;

	switch (size) {
		case "xxs":
			fontSize = 8
			lineHeight = 10
			fontWeight = 700
			break
		case "xs":
			fontSize = 10
			lineHeight = 12
			fontWeight = 400
			break
		case "sm":
			fontSize = 12
			lineHeight = 14
			fontWeight = 400
			break
		case "md":
			fontSize = 14
			lineHeight = 18
			fontWeight = 600
			break
		case "lg":
			fontSize = 16
			lineHeight = 22
			fontWeight = 500
			break
		case "xl":
			fontSize = 18
			lineHeight = 22
			fontWeight = 600
			break
		case "xxl":
			fontSize = 22
			lineHeight = 28
			fontWeight = 700
			break
	}

	return { fontSize, lineHeight, fontWeight }
}

const StyledText = styled.span<{
	fontSize: number
	lineHeight: number
	fontWeight: number
	opacity?: number
}>`
	font-size: ${(props) => props.fontSize}px;
	font-weight: ${(props) => props.fontWeight};
	line-height: ${(props) => props.lineHeight}px;
	color: ${(props) => props.color};
	opacity: ${props => props.opacity};
`

const Text = (props: TextProps) => {
	const { size, color, opacity } = props
	const styles = variant(size)

	return (
		<Fragment>
			<StyledText {...styles} color={color} opacity={opacity}>{props.children}</StyledText>
		</Fragment>
	)
}

Text.defaultProps = {
	color: "#888888"
}

export default Text
