import React from "react"
import styled from "styled-components"
import { ReactComponent as LogoIcon } from "../../assets/logo.svg";
import { ReactComponent as LockIcon } from "../../assets/lock.svg";
import Text from "./Text"
import Flex from "./Flex"

const CardContainer = styled.div`
	background: #ffffff;
	border-radius: 10px;
	padding: 24px;

	.flex {
		margin-bottom: 25px;
	}
`

const Tag = styled(Flex)`
	padding: 4px;
	background: rgb(227,63,55, 0.2);
	border-radius: 2px;
`

const Button = styled.button`
margin-top: 15px;
	width: 100%;
	height: 50px;
	background: linear-gradient(90deg, #00D1C1 0%, #00D1DC 100%);
	border-radius: 4px;
	border: none;
	outline: none;
	cursor: pointer;

	&:hover {
		background: linear-gradient(90deg, #018d81 0%, #00D1DC 100%);
	}

	&:active {
		background: linear-gradient(90deg, #018d81 0%, #00D1DC 100%);
		border: 1px solid #0badb6;
	}
`

const Card = () => {
	return (
		<CardContainer className="card">
			<Flex align="space-between" className="flex">
				<LogoIcon />
				<Tag>
					<Text size="xxs" color="#E33F37">
						BILL OVERDUE
					</Text>
				</Tag>
			</Flex>
			<Flex align="space-between">
				<Flex direction="column">
					<Text size="md">SPENT TILL NOW</Text>
					<Text size="xxl" color="#000000">
						₹1800
					</Text>
				</Flex>
				<Flex direction="column">
					<Text size="md">DUE DATE</Text>
					<Text size="lg" color="#000000">
						15 Aug
					</Text>
				</Flex>
			</Flex>
			<Button>
				<Flex align="center">
					<LockIcon />
					<Text size="lg" color="#FFFFFF">
						PAY ₹1500
					</Text>
				</Flex>
			</Button>
		</CardContainer>
	)
}

export default Card
