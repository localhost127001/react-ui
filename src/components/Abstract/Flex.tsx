import React, { ReactNode } from "react"
import styled from "styled-components"

type FlexProps = {
	align?: "center" | "space-between" | "space-around" | "space-evenly"
	children: ReactNode
	direction?: "row" | "column"
	className?: string
}

export const StyledFlex = styled.div<FlexProps>`
	display: flex;
	align-items: ${(props) =>
		props.direction === "row" || props.align === "center" ? "center" : "flex-start"};
	justify-content: ${(props) => props.align};
	flex-direction: ${(props) => props.direction};
`

const Flex = (props: FlexProps) => {
	return <StyledFlex {...props} className={props.className}>{props.children}</StyledFlex>
}

Flex.defaultProps = {
	align: "center",
	direction: "row",
}

export default Flex
