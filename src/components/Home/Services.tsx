import React from "react"
import styled from "styled-components"
import Text from "../../components/Abstract/Text"
import Flex from "../Abstract/Flex"
import { ReactComponent as Prepaid } from "../../assets/prepaid.svg"
import { ReactComponent as Electricity } from "../../assets/electricity.svg"
import { ReactComponent as Broadband } from "../../assets/broadband.svg"
import { ReactComponent as Dth } from "../../assets/dth.svg"
import { ReactComponent as Gas } from "../../assets/gas.svg"
import { ReactComponent as Landline } from "../../assets/landline.svg"
import { ReactComponent as Water } from "../../assets/water.svg"
import { ReactComponent as Datacard } from "../../assets/datacard.svg"

export const SectionContainer = styled.div`
	background: #ffffff;
  box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.06);
  margin-bottom: 16px;
`

export const SectionHeader = styled.div`
	padding: 8px 16px;
	border-bottom: 2px solid #e0e0e0;
`

const Body = styled.div`
  padding: 16px 25px;
  
  .flex {
    margin-bottom: 25px;
  }

  svg {
    margin-bottom: 6px;
  }
`

const Services = () => {
	return (
		<SectionContainer>
			<SectionHeader>
				<Text size="sm">Recharge & pay bills</Text>
			</SectionHeader>
			<Body>
				<Flex align="space-between" className="flex">
					<Flex direction="column" align="center">
						<Prepaid />
						<Text size="sm">Prepaid</Text>
					</Flex>
					<Flex direction="column" align="center">
						<Electricity />
						<Text size="sm">Electricity</Text>
					</Flex>
					<Flex direction="column" align="center">
						<Broadband />
						<Text size="sm">Broadband</Text>
					</Flex>
					<Flex direction="column" align="center">
						<Dth />
						<Text size="sm">DTH</Text>
					</Flex>
				</Flex>
				<Flex align="space-between">
					<Flex direction="column" align="center">
						<Gas />
						<Text size="sm">Gas</Text>
					</Flex>
					<Flex direction="column" align="center">
						<Landline />
						<Text size="sm">Landline</Text>
					</Flex>
					<Flex direction="column" align="center">
						<Water />
						<Text size="sm">Water</Text>
					</Flex>
					<Flex direction="column" align="center">
						<Datacard />
						<Text size="sm">Data Card</Text>
					</Flex>
				</Flex>
			</Body>
		</SectionContainer>
	)
}

export default Services
