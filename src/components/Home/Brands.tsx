import React from "react"
import styled from "styled-components"
import Text from "../../components/Abstract/Text"
import Flex from "../Abstract/Flex"
import { ReactComponent as Zomato } from "../../assets/zomato.svg"
import { SectionContainer, SectionHeader } from "./Services"

const Body = styled.div`
	padding: 16px 18px 20px 18px;
	display: flex;
	overflow-x: auto;
	&::-webkit-scrollbar {
		width: 0px;
		background: transparent;
	}

	.flex {
		margin-right: 25px;
	}
	margin-bottom: 16px;
`

const Wrapper = styled(Flex)`
	border-radius: 50%;
	width: 56px;
	background-color: #f6f6f6;
	height: 56px;
`

const Services = () => {
	return (
		<SectionContainer>
			<SectionHeader>
				<Text size="sm">You can also use Simpl on</Text>
			</SectionHeader>
			<Body>
				<Flex direction="column" align="center" className="flex">
					<Wrapper>
						<Zomato />
					</Wrapper>
					<Text size="sm">Zomato</Text>
				</Flex>
				<Flex direction="column" align="center" className="flex">
					<Wrapper>
						<Zomato />
					</Wrapper>
					<Text size="sm">Grofers</Text>
				</Flex>
				<Flex direction="column" align="center" className="flex">
					<Wrapper>
						<Zomato />
					</Wrapper>
					<Text size="sm">Dunzo</Text>
				</Flex>
				<Flex direction="column" align="center" className="flex">
					<Wrapper>
						<Zomato />
					</Wrapper>
					<Text size="sm">Dunzo</Text>
				</Flex>
				<Flex direction="column" align="center" className="flex">
					<Wrapper>
						<Zomato />
					</Wrapper>
					<Text size="sm">Bigbasket</Text>
				</Flex>
			</Body>
		</SectionContainer>
	)
}

export default Services
