import React from "react"
import styled from "styled-components"
import Text from "../Abstract/Text"
import { ReactComponent as SimplIcon } from "../../assets/simpl.svg"
import Card from "../Abstract/Card"

const Header = styled.div`
	position: relative;
	margin-bottom: 130px;

	.rect {
		height: 25px;
		background-color: #02bab0;
	}

	.scroll {
		&::-webkit-scrollbar {
			width: 0px;
			background: transparent;
		}
		margin-top: 8px;
		overflow-x: auto;
		display: flex;
		.card {
			min-width: 60%;
		}
		.card:first-child {
			margin-right: 16px;
		}
	}
`

const Section = styled.div`
	height: 207px;
	background-color: #00d1c1;
	padding: 0 16px;
`

const Title = styled.div`
	display: flex;
	align-items: center;
	margin-bottom: 15px;
	padding-top: 25px;
	span {
		margin-right: 6px;
	}
`

const Accounts = () => {
	return (
		<Header>
			<div className="rect"></div>
			<Section>
				<Title>
					<Text size="xl" color="#FFFFFF">
						Welcome to
					</Text>
					<SimplIcon />
				</Title>
				<Text size="md" color="#FFFFFF" opacity={0.8}>
					2 Active accounts
				</Text>
				<div className="scroll">
					<Card />
					<Card />
				</div>
			</Section>
		</Header>
	)
}

export default Accounts
