import React from "react"
import styled from "styled-components"
import Text from "../Abstract/Text"
import Flex from "../Abstract/Flex"
import { ReactComponent as Zomato } from "../../assets/zomato.svg"
import { SectionContainer, SectionHeader } from "./Services"

const Body = styled.div`
	.flex {
		padding: 20px 16px;
		border-bottom: 1px solid #e0e0e0;
		span:first-child {
			margin-bottom: 2px;
		}

		span:nth-child(3n) {
			margin-top: 8px;
		}
	}

	.flex-text {
		span:first-child {
			margin-top: 4px;
			margin-right: 2px;
		}
	}
	margin-bottom: 32px;
`

const Wrapper = styled.div`
	display: flex;
	svg {
		margin-right: 16px;
	}
`

const Tag = styled.div`
	background: rgb(0, 209, 193, 0.1);
  padding: 3px 4px;
  border-radius: 4px;
  display: flex;
  margin-left: 15px;
`

const Transactions = () => {
	return (
		<SectionContainer>
			<SectionHeader>
				<Text size="sm">Recent transactions</Text>
			</SectionHeader>
			<Body>
				<Flex align="space-between" className="flex">
					<Wrapper>
						<Zomato />
						<Flex direction="column" align="space-around">
							<Text size="md" color="#000000">
								Zomato
							</Text>
							<Flex>
								<Text size="sm" color="#000000">
									May 24
								</Text>
								<Text size="sm">, 06:12 PM | Simpl Pay Later</Text>
							</Flex>
						</Flex>
					</Wrapper>
					<Text size="md" color="#000000">
						₹250
					</Text>
				</Flex>

				<Flex align="space-between" className="flex">
					<Wrapper>
						<Zomato />
						<Flex direction="column" align="space-around">
							<Flex>
								<Text size="md" color="#000000">
									Bescom
								</Text>
								<Tag>
									<Text size="xxs" color="#00A699">
										AUTOPAY
									</Text>
								</Tag>
							</Flex>
							<Flex>
								<Text size="sm" color="#000000">
									May 24
								</Text>
								<Text size="sm">, 06:10 PM</Text>
							</Flex>

							<Text size="sm">Home - 0090887667</Text>
						</Flex>
					</Wrapper>
					<Text size="md" color="#000000">
						₹250
					</Text>
				</Flex>

				<Flex align="space-between" className="flex">
					<Wrapper>
						<Zomato />
						<Flex direction="column" align="space-around">
							<Text size="md" color="#000000">
								Zomato
							</Text>
							<Flex>
								<Text size="sm" color="#000000">
									May 23
								</Text>
								<Text size="sm">, 06:10 PM</Text>
							</Flex>
							<Flex className="flex-text">
							<Text size="sm" color="#e36634">
								₹100 Cashback !  
							</Text>
								<Text size="sm" color="#494949">
								  Will be  credited after repayment
							</Text>
							</Flex>
						</Flex>
					</Wrapper>
					<Text size="md" color="#000000">
						₹300
					</Text>
				</Flex>
			</Body>
		</SectionContainer>
	)
}

export default Transactions
