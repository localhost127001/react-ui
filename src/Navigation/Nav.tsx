import React from "react"
import { ReactComponent as HomeIcon } from "../assets/home.svg"
import { ReactComponent as BillboxIcon } from "../assets/billbox.svg"
import { ReactComponent as PassbookIcon } from "../assets/passbook.svg"
import { ReactComponent as ProfileIcon } from "../assets/profile.svg"
import { ReactComponent as MoreIcon } from "../assets/more.svg"
import styled from "styled-components"
import { NavLink } from "react-router-dom"
import Flex from "../components/Abstract/Flex"

const NavContainer = styled(Flex)`
	max-width: 360px;
	width: 100%;
	height: 56px;
	margin: auto;
	position: fixed;
	bottom: 0;
	left: 0;
	right: 0;
	background: #FFFFFF;
	box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.07);
	border-top: 1px solid #E0E0E0;
	
	.nav-icons {
		height: 56px;
		flex-basis: 20%;
		display: flex;
		align-items: center;
		justify-content: center;
	}

	.hurray {
		border-top: 2px solid #00D1C1;
	}
`

const Nav = () => {
	return (
		<NavContainer align="space-around">
			<NavLink to="/home" className="nav-icons" activeClassName="hurray">
				<HomeIcon />
			</NavLink>
			<NavLink to="/billbox" className="nav-icons" activeClassName="hurray">
				<BillboxIcon />
			</NavLink>
			<NavLink to="/passbook" className="nav-icons" activeClassName="hurray">
				<PassbookIcon />
			</NavLink>
			<NavLink to="/profile" className="nav-icons" activeClassName="hurray">
				<ProfileIcon />
			</NavLink>
			<NavLink to="/more" className="nav-icons" activeClassName="hurray">
				<MoreIcon />
			</NavLink>
		</NavContainer>
	)
}

export default Nav
